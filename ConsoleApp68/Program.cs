﻿using RazorLight;
using System;
using System.IO;
using System.Threading.Tasks;

namespace ConsoleApp68
{
    public class MyModel
    {
        public string Name { get; set; }
    }

    internal static class Program
    {
        private static async Task Main(string[] args)
        {
            var engine = new RazorLightEngineBuilder()
                          .UseFilesystemProject(Directory.GetCurrentDirectory())
                          .Build();

            var result = await engine.CompileRenderAsync("Views/SynonymConfig.cshtml", new MyModel { Name = "John Doe" });

            Console.WriteLine(result);
        }
    }
}